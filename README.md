# dev_web

adresse du site (always data): http://zoolandia.alwaysdata.net/Dev_Web/
équivalent à ce qu'il y a dans final_version

Si jamais vous téléchargez le dossier 'final_version'
alors vous aurez les derniers code source, ceux qui sont à jour
et pour la compilation:
localhost: chemin où se trouve le repertoire final_version/Dev_Web/
normalement vous verrez la page index.php dans laquelle on voit les différents secteurs et une carte du zoo.

Dans le dossier hashed_pwd vous trouverez les différents utilisateurs et un fichier .txt dans lequel vous trouverez les mots de passe hashé de quelques utilisateurs.

Dans le dossier BDD vous trouverez les tables et leurs relations.

Dans les dossier mission vous trouverez les missions en lien avec le site de Mr.Freebourg, ces fichiers ne sont tous pas à jour.

Dans le dossier use_case vous trouverez le diagramme d'utilisation du site.

Je vous souhaite une bonne visite  :-)
