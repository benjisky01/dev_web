function updateWeather(){
    fetch('./process.php')
    .then(function(response) {
      return response.json();
    })
    .then(function(json) {
        
        // json.time 
        var newText = document.createTextNode(json.time);
        var timeP = document.getElementById("time"); 
        while(timeP.childNodes.length > 0){
            timeP.removeChild(timeP.childNodes[0]);
        }
        timeP.appendChild(newText);
        
              
    });   
    setTimeout(updateWeather, 1000);
}

window.onload = updateWeather;