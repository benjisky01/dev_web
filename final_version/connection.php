<?php

//My connection Information
/*
$username = "root";
$password ="password";
$servername = "localhost:3308";
$db_name = "zoolandia";
*/

//always Data information
$username = "zoolandia";
$password = "password";
$servername = "mysql-zoolandia.alwaysdata.net";
$db_name ="zoolandia_db";

/* Mr grignon's information (if you want to try :-) you must import zoolandia database)
below information are taken from teams/Web/password_hash-verify/connection.inc.php
$username = "root";
$password = "mySQLRomu78$";
$servername = "localhost";

*/

try {
    $conn = new PDO("mysql:host=$servername;dbname=$db_name", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }


?>
